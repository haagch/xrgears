/*
 * xrgears
 *
 * Copyright 2020 Collabora Ltd.
 *
 * Authors: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#include "ktx_texture.h"

#ifdef __cplusplus
extern "C" {
#endif

ktx_size_t
rooftop_size(void);
const ktx_uint8_t*
rooftop_bytes(void);

ktx_size_t
cat_size(void);
const ktx_uint8_t*
cat_bytes(void);

ktx_size_t
hawk_size(void);
const ktx_uint8_t*
hawk_bytes(void);

#ifdef __cplusplus
}
#endif
